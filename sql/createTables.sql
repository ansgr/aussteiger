DROP TABLE IF EXISTS `sectsgermany`;
CREATE TABLE sectsgermany (
    `id` INTEGER(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `description` VARCHAR(150),
    `membercount` INTEGER(11));

INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Neuapostolische Kirche", 445000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Zeugen Jehovas", 192000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Mormonen", 36000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Vereinigte", 30000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Apostolische Gemeinden", 8000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Neugermanen", 25000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Christengemeinschaft", 20000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Bruno-Gröning", 12000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Apostolische Gemeinschaft", 8000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Christliche Wissenschaft", 8000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Universelles Leben", 6000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Bahai", 5000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Apostelamt Juda", 4500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Johannische Kirche", 3500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Gralsbewegung", 2500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Soka Gakkai", 2100);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Boston Church of Christ", 1000);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Rajneesh Bewegung", 750);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Fia Lux", 700);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Vereinigungskirche", 500);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("ISKON (Krishna)", 390);
INSERT INTO `sectsgermany` (`description`, `membercount`) VALUES ("Scientology", 5500);

DROP TABLE IF EXISTS `stories`;
CREATE TABLE stories (
    `id` INTEGER(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `header` VARCHAR(250),
    `name` VARCHAR(150),
    `text` VARCHAR(10000),
    `date` DATETIME,
    `img` VARCHAR(150));

INSERT INTO `stories` (`date`, `header`, `name`, `text`) VALUES ("2018-05-12", "Mein Leben und mein Ausstieg bei den Zeugen Jehovas", "Dietlinde G.",
"Hallo zusammen,<br />
nach langer Überlegung schreibe ich hier doch mal meine Geschichte.<br />
<br />
Ich wurde zu den Zeugen rein geboren. Heißt, sowohl mein Vater als auch meine Mutter waren damals getauft. Als Kind war das alles normal für mich. Ich kannte es ja nicht anders. <br />
<br />
Ich war ein sehr schüchternes Kind, habe mich immer hinter meiner Mutter versteckt und könnte nicht mit fremden Menschen sprechen. <br />
<br />
Im Kindergarten war ich nicht, da es bei uns im Dorf nur einen kirchlichen gab. Meine Leidenszeit fing an als ich in die Schule kam. Alle kannten sich schon aus dem Kindergarten, nur mich kannte niemand. Natürlich wurde auch gleich verkündet dass ich morgens nicht mit den anderen beten durfte (bei uns in der Schule würde noch jeden morgen das Vaterunser gebetet) und das ich keine Geburtstage feiere. Damit hatte ich in unserer kleinen Dorfschule natürlich gleich einen Ruf weg.<br />
<br />
 Trotzdem kam ich recht gut durch die Grundschule und fand auch ziemlich viele „weltliche“ Freunde. Meine Eltern waren für Zeugen ziemlich locker und es wurde mir weder vorgeschrieben welche Freunde ich haben muss, noch wurde ich dazu gezwungen in den Dienst zu gehen oder so.<br />
<br />
Trotzdem war die Erwartungshaltung natürlich nicht nur von meinen Eltern, sondern auch von den Ältesten die dass ich ein guter Zeuge werde. Am Anfang sah auch alles danach aus. Ich identifizierte mich mit der Versammlung, ging predigen (Auch wenn ich jedes mal hoffte keinem Klassenkameraden zu begegnen) und wollte mich auch immer mehr in die Versammlung einbringen , mit Aufgaben usw.<br />
<br />
Aber es gab schon immer die andere Seite. Es war mir furchtbar peinlich, dass ich in der Schule nicht gratulieren durfte wenn jemand Geburtstag hatte. Wenn ich wieder an Weihnachten oder St. Martin nicht mit basteln durfte. Wenn die Anderen mit ihren Laternen durch die Straßen zogen. Ich wollte schon immer gern dabei sein. Einmal gratulierte ich meinem Klassenkameraden zum Geburtstag weil ich nicht sagen wollte dass ich das nicht darf. Ich hatte so ein schlechtes Gewissen danach. Ich habe es dann natürlich meiner Mutter erzählt und es gab ein ernstes Gespräch mit meinen Eltern.<br />
<br />
Natürlich wurde ich auch ab und an von meinen Eltern geschlagen, nicht schlimm verprügelt sondern mehr Ohrfeigen, was ich aber trotzdem als sehr schlimm und erniedrigend empfand.<br />
<br />
Ab der weiterführenden Schule habe ich mich stark verändert. Ich wurde selbstbewusster und frecher, fing an langsam zu rebellieren. Das entging natürlich den anderen in der Versammlung nicht und ich wurde auch da zum Außenseiter. Ich war bei den anderen Kindern in der Versammlung ein bisschen das „cool kid“ und wurde einerseits ein bisschen bewundert aber trotzdem wollte natürlich niemand mehr etwas mit mir zu tun haben.<br />
<br />
Meine Mutter ließ sich ausschließen als ich vielleicht 12 war. Ehrlich gesagt weiß ich gar nicht ob sie ausgeschlossen ist, jedenfalls ist sie nicht mehr hin gegangen. Danach mussten wir Kinder uns entscheiden. Eine Zeitlang bin ich mit meinem Vater noch regelmäßig in die Versammlung gegangen. Dann irgendwann nur noch zu Kongressen und zum Gedächtnismahl. Mit glaube ich 15 bin ich dann gar nicht mehr gegangen. Mein Vater fragte zwar immer ob wir mitkommen wollten aber er zwang uns zu nichts. <br />
<br />
Als ich dann mit 18 zuhause ausgezogen bin, habe ich es erstmal richtig krachen lassen. Ich war praktisch nur noch betrunken, bin wahllos mit irgendwelchen Typen ins Bett, habe Drogen genommen, meine Schule abgebrochen, bin mit Punks auf der Straße ein gehangen. Ich war kurz vorm abrutschen und hab dann zum Glück doch noch die Kurve gekriegt. Ich bereue nichts, außer dass ich damals vielen Menschen weh getan habe. Seitdem haben sich meine Tanten und Onkel die noch in der Versammlung waren komplett von mir angewandt. Damit konnte ich leben, ich könnte die sowieso noch nie leiden.<br />
<br />
Allgemein bin ich sehr froh,  dass meine Eltern doch einigermaßen locker waren und das obwohl mein Vater ein sehr hoher Ältester war. Trotzdem habe ich immer noch große Probleme Nähe zuzulassen und meine Gefühle zu äußern. Es gab in unserer Familie keine Nähe. Es gab keine Umarmungen oder liebe Worte. Als mein Vater letztes Jahr überraschend starb, haben mir viele junge Zeugen gesagt, er wäre wie ein Vater für sie gewesen. Das hat mir sehr weh getan, denn für mich war er es nie. Er hat sich immer mehr um die Versammlung gekümmert als um die Familie. Ständig mussten wir zurück stecken weil er in den Dienst müsste oder einen Vortrag halten. <br />
<br />
Eine weitere große Angst von mir ist immer dass irgendjemand erfahren könnte dass ich mal bei den Zeugen war. Aus meinem jetzigen Umfeld weiß das niemand und ich möchte nicht dass es jemand erfährt. Aber andererseits habe ich das Gefühl dass ob mit jemandem darüber sprechen muss, aber ich denke auch dass jemand der nie dabei war, mich einfach nicht verstehen kann. Deswegen bin ich jetzt hier und auch wenn meine Geschichte vielleicht nicht so schlimm ist wie andere hier, setzt es mir doch immer noch sehr zu.<br />
<br />
LG Dietlinde");

INSERT INTO `stories` (`date`, `header`, `name`, `text`) VALUES ("2015-04-09", "Hilfe beim Austieg der Freundin", "Rainer W.",
"Hallo Zusammen,<br>
<br>ich habe gestern geschrieben, dass meine Freundin / Verlobte (3 KInder; ungetaufte Zeugin seit 10 Jahren; kam hin nach häuslicher Gewalt durch den Ex. Arbeitskollegin sprach Sie dann an) und mit der ich seit 2 Jahren inkl. regelmäßigem Sex zusammenwohne, sich wieder intensiv den Zeugen zugewandt hat. Dies nachdem Sie innerhalb eines Jahre nur 4x dort war (ohne auch nur einen Kongress zu besuchen!). Ich hatte nicht gegen die ZJ gearbeitet, sondern ihr Selbstwertgefühl gesteigert und ihr mehr Freiheit gegeben. Ich bin dann zum Nachdenken ausgezogen, damit wir über uns nachdenken können. Der Rückfall kam durch zwei Problemfälle (älteste Tochter ging mit 15 Jahren wieder zum Vater, der alle schlug und Sie muss aufstocken beim Amt). Nach dem 3 Tageskongress vor 2 Wochen war Sie dann komplett wieder \"auf Droge\" (will wieder in die Wahrheit =&gt; kein Sex, kein Zusammenwohnen vor der Ehe, viel von Hauis zu Haus gehen, will immer in jede Versammlung, jeden Kongress. Auch die Kleine mit 7 Jahren muss seit 4 Wochen Bibelstudium machen). Beweise in der Bibel findet Sie für die Behauptungen kein Sex vor der Ehe natürlich nicht. Sie betreibt auch keine Hurerei, wie ihr eingeedt wird. Natürlich ist Sie Artikel und Infos aus dem Internet nicht zugänglich (Satanszeug). Findet aber keine Beweise in der Bibel (auch nicht über ihre \"Bibelstudiumtanten\"). Aussagen: Das weis doch jedes Kind lies ich nicht gelten.<br>
<br>
Hat jemand gute Ideen, ob es noch eine Chance gibt und was ich tun könnte?<br>
<br>
<b>Meine Überlegungen sind folgende</b>:<br>
Ich gehe mit einem Brief über die Mutter (Inhalt: was machen die Zeugen, weshalb liebe ich die Tochter, wie hat diese sich verändert, ich mag ihre drei Kinder und will nicht, das diese auch \"versaut werden fürs Leben\"), die keine Zeugin ist. <b>Wie kann diese überzeugen?</b><br />Ich schreibe ihr einen emotionalen, aber ergebnisoffener Brief. Inhalt: Liebe dich, beide brauchen mehr Freiräume, haben beide Fehler gemacht, die positiven Dinge überwiegen, ich mag an Dir: Nähe, Aussehen, lachen, reden können,.... Lass uns in 6 Wochen (nach ihrer Mutter-Kind Kur) zusammensetzen und die Zukunft besprechen<br />Ich gehe über Ihre Cousine (keine Zeugin), mit der Sie früher oft zusammen war und die mit ihr reden will (<b>Sie macht es. Wie berzeugt Sie?)</b><br />Die Älteste Tochter, die die eifrigste Zeugin war (ungetaufte Verkünderin), geht dort nicht mehr jin (wollte dies seit 2 Jahren nicht, da Sie Zweifel hatte ide ich bestärkte). Blieb aber wegen der Mutter dort. Erwähnte bei der Mutter nun, dass dies aus eigener Überzeugung geschah, und nicht weil die Freundin nicht mehr in die Versammlung ging (das denkt diese !). Bei der Mutter-Kind Kür würde die Älteste ggf. die Mittlere Tochter / Mutter auf das thema ansprechen. <b>Wie sollte das geschehen?</b><br>
Vielen Dank schon im Voraus für eure Anregungen<br>
Liebe Grüße<br>
<br>
Rainer W.");