// ReSharper disable once InconsistentNaming
var AlertNoData = "Es konnten nicht alle Daten geladen werden";
function getThreeStories() {
    $.get("/ThreeStories", function (data) {
        indexViewState.stories = JSON.parse(data);
        for (var i = 0; i < indexViewState.stories.length; i++) {
            indexViewState.stories[i].Date = convertDateStringToDate(indexViewState.stories[i].Date);
        }
        return;
    }).fail(function () {
        alert(AlertNoData);
    });
}
function convertDateStringToDate(dateString) {
    return dateString.substring(8, 10) + "." + dateString.substring(5, 7) + "." + dateString.substring(0, 4);
}
//# sourceMappingURL=IndexFunctions.js.map