﻿// ReSharper disable InconsistentNaming
class Story {
    ID: number;
    Header: string;
    Name: string;
    Text: string;
    Date: string;
    ImG: number;
}