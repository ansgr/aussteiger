﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aussteiger.Components.Communication;
using aussteiger.Components.Persistence;

namespace aussteiger.Appstart
{
    class Program
    {
        static void Main(string[] args)     
        {
            Console.WriteLine($"aussteiger | {DateTime.Now} | starting. . .");

            var persistenceManager = new PersistenceManager();

            var communicationManager = new CommunicationManager(persistenceManager);
            
            communicationManager.Start();

            Console.WriteLine($"aussteiger | {DateTime.Now} | stopped");
        }
    }
}
