﻿// ReSharper disable InconsistentNaming
namespace aussteiger.DataObject
{
    public class HttpMethods
    {
        public const string GET = "GET";
        public const string POST = "POST";
        public const string DELETE = "DELETE";
        public const string PUT = "PUT";
    }
}
