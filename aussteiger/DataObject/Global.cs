﻿using System;
using System.IO;
using System.Text;

using aussteiger.Utils;

namespace aussteiger.DataObject
{
    public static class Global
    {
        public const string CookieName = "Aussteiger-Session";
        private static Settings _settings;
        public static string Url => $"http://{Environment.MachineName}:{Settings.Port}";

        public static Settings Settings
        {
            get
            {
                if (_settings == null)
                {
                    DeserializeSettings();
                }
                return _settings;
            }
        }

        private static void DeserializeSettings()
        {
            Console.WriteLine("Initializing settings");
            const string settingFile = "settings.json";

            if (File.Exists(settingFile))
            {
                try
                {
                    Console.WriteLine("deserializing settings file");
                    var json = File.ReadAllText(settingFile, Encoding.UTF8);
                    _settings = SerializationUtility.DeserializeObject<Settings>(json);
                }
                catch (Exception e)
                {
                    Console.WriteLine("deserializing settings failes");
                    Console.WriteLine(e);
                    FallBackToStandardSettings();
                }
            }
            else
            {
                Console.WriteLine($"deserializing settings failes: File does not exist");
                FallBackToStandardSettings();
            }

            var message = $"loaded settings: {Environment.NewLine}{_settings}";
            Console.WriteLine(message);
            Console.WriteLine(message);
        }

        private static void FallBackToStandardSettings()
        {
            Console.WriteLine("Falling back to standart settings");
            _settings = new Settings
            {
                //ToDo change to final standart settings
                Port = "7777",
                Server = "localhost",
                Database = "aussteiger",
                UserID = "root",
                Password = "1234"
            };
        }
    }
}
