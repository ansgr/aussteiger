﻿using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace aussteiger.Utils
{
    public static class SerializationUtility
    {
        public static string SerializeObject(object obj)
        {
            return JsonConvert.SerializeObject(obj, SerializerSettings);
        }

        public static T DeserializeObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, SerializerSettings);
        }

        private static JsonSerializerSettings SerializerSettings => _serializerSettings ?? (_serializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            FloatParseHandling = FloatParseHandling.Decimal,
            Converters = new List<JsonConverter>
            {
                new StringEnumConverter()
            }
        });

        private static JsonSerializerSettings _serializerSettings;
    }
}
