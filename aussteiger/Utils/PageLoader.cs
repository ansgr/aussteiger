﻿using System;
using System.Collections.Generic;
using System.IO;

namespace aussteiger.Utils
{
    class PageLoader
    {
        private static PageLoader _instance;
        private static readonly object LockObject = new object();
        private readonly Dictionary<string, byte[]> _urlToWebPage = new Dictionary<string, byte[]>();

        public static PageLoader Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                lock (LockObject)
                {
                    return _instance ?? (_instance = new PageLoader());
                }
            }
        }

        private PageLoader()
        {
            LoadPages();
        }

        private void LoadPages()
        {
            var dirInfo = new DirectoryInfo(".\\FrontEnd");
            foreach (var fileInfo in dirInfo.GetFiles("*.*", SearchOption.AllDirectories))
            {
                var pageUrl = fileInfo.FullName.Replace(dirInfo.FullName, "").Replace("\\", "/").TrimStart('/');
                _urlToWebPage.Add(pageUrl.ToLower(), File.ReadAllBytes(fileInfo.FullName));
                Console.WriteLine($"PL | {DateTime.Now} | Page loaded :: {pageUrl}");
            }
        }

        public byte[] GetPage(string url)
        {
            return _urlToWebPage[url.ToLower()];
        }

        public bool ContainsPage(string url)
        {
            return _urlToWebPage.ContainsKey(url.ToLower());
        }
    }
}
