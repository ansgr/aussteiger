﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using aussteiger.Attributes;
using aussteiger.Components.Persistence;
using aussteiger.Components.Persistence.ORM;
using aussteiger.DataObject;
using aussteiger.DataObject.REST;
using aussteiger.Utils;


// ReSharper disable UnusedMember.Global
namespace aussteiger.Components.Communication
{
    public class RestInterface
    {
        private readonly HttpListenerContext _ctx;
        private readonly PersistenceManager _persistenceManager;
        private readonly SessionManager _sessionManager;
        
        public RestInterface(HttpListenerContext ctx, SessionManager sessionManager, PersistenceManager persistenceManager)
        {
            _ctx = ctx;
            _persistenceManager = persistenceManager;
            _sessionManager = sessionManager;
        }


        #region Storie

        [UrlMapping("ThreeStories", HttpMethods.GET)]
        public void GettAllRegistrations()
        {
            Console.WriteLine($"CM | {DateTime.Now} | Get all registrations");
            var stories = _persistenceManager.GetThreeStories();
            ResponseWriter.WriteText(_ctx.Response, SerializationUtility.SerializeObject(stories));
        }

        #endregion
    }
}
