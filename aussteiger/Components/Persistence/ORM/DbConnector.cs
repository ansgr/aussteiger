﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aussteiger.DataObject;
using DbLinq.Data.Linq;
using MySql.Data.MySqlClient;

namespace aussteiger.Components.Persistence.ORM
{
    public class DbConnector
    {
        private readonly string _connectionString;

        #region general

        public DbConnector()
        {
            _connectionString = $"SERVER={Global.Settings.Server};DATABASE={Global.Settings.Database};UID={Global.Settings.UserID};PASSWORD={Global.Settings.Password};";
        }

        public bool TryToConnect()
        {
            try
            {
                Console.WriteLine("Try to connect to database");
                using (var connection = new MySqlConnection(_connectionString))
                {
                    connection.Open();
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while connecting");
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        private async Task<bool> ExecuteCommandAsync(string cmdText)
        {
            try
            {
                Console.WriteLine($"Excecute command: \"{cmdText}\"");
                using (var connection = new MySqlConnection(_connectionString))
                {
                    await connection.OpenAsync();
                    var command = new MySqlCommand(cmdText, connection);
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while executing");
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        private string InsertCommand(string key, string values)
        {
            switch (key)
            {
                default:
                    Console.WriteLine($"No InsertInto statement defined for key {key}");
                    return null;
            }
        }

        public object GetObjectById(int id, string type)
        {
            Console.WriteLine($"GetObjectByID => {id} :: {type}");
            try
            {
                using (var dataContext = new DataContext(new MySqlConnection(_connectionString)))
                {
                    switch (type.ToLower())
                    {
                        default:
                            Console.WriteLine("Unknown type");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getting object by ID");
                Console.WriteLine(e);
            }
            return null;
        }


        #endregion
    }
}
