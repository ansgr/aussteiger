﻿// 
//  ____  _     __  __      _        _ 
// |  _ \| |__ |  \/  | ___| |_ __ _| |
// | | | | '_ \| |\/| |/ _ \ __/ _` | |
// | |_| | |_) | |  | |  __/ || (_| | |
// |____/|_.__/|_|  |_|\___|\__\__,_|_|
//
// Auto-generated from aussteiger on 2018-09-20 10:52:51Z.
// Please visit http://code.google.com/p/dblinq2007/ for more information.
//

using System.Data.Common;

namespace aussteiger.Components.Persistence.ORM
{
    using System;
    using System.ComponentModel;
    using System.Data;
#if MONO_STRICT
	using System.Data.Linq;
#else   // MONO_STRICT
    using DbLinq.Data.Linq;
    using DbLinq.Vendor;
#endif  // MONO_STRICT
    using System.Data.Linq.Mapping;
    using System.Diagnostics;


    public partial class Aussteiger : DataContext
    {

        #region Extensibility Method Declarations
        partial void OnCreated();
        #endregion


        public Aussteiger(string connectionString) :
                base(connectionString)
        {
            this.OnCreated();
        }

        public Aussteiger(string connection, MappingSource mappingSource) :
                base(connection, mappingSource)
        {
            this.OnCreated();
        }

        public Aussteiger(IDbConnection connection, MappingSource mappingSource) :
                base((DbConnection) connection, mappingSource)
        {
            this.OnCreated();
        }

        public Table<SectsGermany> SectsGermany
        {
            get
            {
                return this.GetTable<SectsGermany>();
            }
        }

        public Table<Stories> Stories
        {
            get
            {
                return this.GetTable<Stories>();
            }
        }
    }

    #region Start MONO_STRICT
#if MONO_STRICT

	public partial class AUssTeIGeR
	{
		
		public AUssTeIGeR(IDbConnection connection) : 
				base(connection)
		{
			this.OnCreated();
		}
	}
    #region End MONO_STRICT
    #endregion
#else     // MONO_STRICT

    public partial class Aussteiger
    {

        public Aussteiger(IDbConnection connection) :
                base((DbConnection) connection, new DbLinq.MySql.MySqlVendor())
        {
            this.OnCreated();
        }

        public Aussteiger(IDbConnection connection, IVendor sqlDialect) :
                base((DbConnection) connection, sqlDialect)
        {
            this.OnCreated();
        }

        public Aussteiger(IDbConnection connection, MappingSource mappingSource, IVendor sqlDialect) :
                base((DbConnection) connection, mappingSource, sqlDialect)
        {
            this.OnCreated();
        }
    }
    #region End Not MONO_STRICT
    #endregion
#endif     // MONO_STRICT
    #endregion

    [Table(Name = "aussteiger.sectsgermany")]
    public partial class SectsGermany : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
    {

        private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");

        private string _description;

        private int _id;

        private System.Nullable<int> _memberCount;

        #region Extensibility Method Declarations
        partial void OnCreated();

        partial void OnDescriptionChanged();

        partial void OnDescriptionChanging(string value);

        partial void OnIDChanged();

        partial void OnIDChanging(int value);

        partial void OnMemberCountChanged();

        partial void OnMemberCountChanging(System.Nullable<int> value);
        #endregion


        public SectsGermany()
        {
            this.OnCreated();
        }

        [Column(Storage = "_description", Name = "description", DbType = "varchar(150)", AutoSync = AutoSync.Never)]
        [DebuggerNonUserCode()]
        public string Description
        {
            get
            {
                return this._description;
            }
            set
            {
                if (((_description == value)
                            == false))
                {
                    this.OnDescriptionChanging(value);
                    this.SendPropertyChanging();
                    this._description = value;
                    this.SendPropertyChanged("Description");
                    this.OnDescriptionChanged();
                }
            }
        }

        [Column(Storage = "_id", Name = "id", DbType = "int", IsPrimaryKey = true, IsDbGenerated = true, AutoSync = AutoSync.Never, CanBeNull = false)]
        [DebuggerNonUserCode()]
        public int ID
        {
            get
            {
                return this._id;
            }
            set
            {
                if ((_id != value))
                {
                    this.OnIDChanging(value);
                    this.SendPropertyChanging();
                    this._id = value;
                    this.SendPropertyChanged("ID");
                    this.OnIDChanged();
                }
            }
        }

        [Column(Storage = "_memberCount", Name = "membercount", DbType = "int", AutoSync = AutoSync.Never)]
        [DebuggerNonUserCode()]
        public System.Nullable<int> MemberCount
        {
            get
            {
                return this._memberCount;
            }
            set
            {
                if ((_memberCount != value))
                {
                    this.OnMemberCountChanging(value);
                    this.SendPropertyChanging();
                    this._memberCount = value;
                    this.SendPropertyChanged("MemberCount");
                    this.OnMemberCountChanged();
                }
            }
        }

        public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
            if ((h != null))
            {
                h(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
            if ((h != null))
            {
                h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [Table(Name = "aussteiger.stories")]
    public partial class Stories : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
    {

        private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");

        private System.Nullable<System.DateTime> _date;

        private string _header;

        private int _id;

        private string _imG;

        private string _name;

        private string _text;

        #region Extensibility Method Declarations
        partial void OnCreated();

        partial void OnDateChanged();

        partial void OnDateChanging(System.Nullable<System.DateTime> value);

        partial void OnHeaderChanged();

        partial void OnHeaderChanging(string value);

        partial void OnIDChanged();

        partial void OnIDChanging(int value);

        partial void OnIMgChanged();

        partial void OnIMgChanging(string value);

        partial void OnNameChanged();

        partial void OnNameChanging(string value);

        partial void OnTextChanged();

        partial void OnTextChanging(string value);
        #endregion


        public Stories()
        {
            this.OnCreated();
        }

        [Column(Storage = "_date", Name = "date", DbType = "datetime", AutoSync = AutoSync.Never)]
        [DebuggerNonUserCode()]
        public System.Nullable<System.DateTime> Date
        {
            get
            {
                return this._date;
            }
            set
            {
                if ((_date != value))
                {
                    this.OnDateChanging(value);
                    this.SendPropertyChanging();
                    this._date = value;
                    this.SendPropertyChanged("Date");
                    this.OnDateChanged();
                }
            }
        }

        [Column(Storage = "_header", Name = "header", DbType = "varchar(250)", AutoSync = AutoSync.Never)]
        [DebuggerNonUserCode()]
        public string Header
        {
            get
            {
                return this._header;
            }
            set
            {
                if (((_header == value)
                            == false))
                {
                    this.OnHeaderChanging(value);
                    this.SendPropertyChanging();
                    this._header = value;
                    this.SendPropertyChanged("Header");
                    this.OnHeaderChanged();
                }
            }
        }

        [Column(Storage = "_id", Name = "id", DbType = "int", IsPrimaryKey = true, IsDbGenerated = true, AutoSync = AutoSync.Never, CanBeNull = false)]
        [DebuggerNonUserCode()]
        public int ID
        {
            get
            {
                return this._id;
            }
            set
            {
                if ((_id != value))
                {
                    this.OnIDChanging(value);
                    this.SendPropertyChanging();
                    this._id = value;
                    this.SendPropertyChanged("ID");
                    this.OnIDChanged();
                }
            }
        }

        [Column(Storage = "_imG", Name = "img", DbType = "varchar(150)", AutoSync = AutoSync.Never)]
        [DebuggerNonUserCode()]
        public string IMg
        {
            get
            {
                return this._imG;
            }
            set
            {
                if (((_imG == value)
                            == false))
                {
                    this.OnIMgChanging(value);
                    this.SendPropertyChanging();
                    this._imG = value;
                    this.SendPropertyChanged("IMg");
                    this.OnIMgChanged();
                }
            }
        }

        [Column(Storage = "_name", Name = "name", DbType = "varchar(150)", AutoSync = AutoSync.Never)]
        [DebuggerNonUserCode()]
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                if (((_name == value)
                            == false))
                {
                    this.OnNameChanging(value);
                    this.SendPropertyChanging();
                    this._name = value;
                    this.SendPropertyChanged("Name");
                    this.OnNameChanged();
                }
            }
        }

        [Column(Storage = "_text", Name = "text", DbType = "varchar(10000)", AutoSync = AutoSync.Never)]
        [DebuggerNonUserCode()]
        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                if (((_text == value)
                            == false))
                {
                    this.OnTextChanging(value);
                    this.SendPropertyChanging();
                    this._text = value;
                    this.SendPropertyChanged("Text");
                    this.OnTextChanged();
                }
            }
        }

        public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
            if ((h != null))
            {
                h(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
            if ((h != null))
            {
                h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
