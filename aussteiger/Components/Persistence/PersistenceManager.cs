﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aussteiger.Components.Persistence.ORM;
using DbLinq.Data.Linq;

namespace aussteiger.Components.Persistence
{
    public class PersistenceManager
    {
        private readonly object LockObject = new object();

        private string TrimRqBody(string rqBody)
        {
            return rqBody.Trim('\"');
        }

        #region stories

        

        #endregion

        public List<Stories> GetThreeStories()
        {
            lock (LockObject)
            {
                using (var ctx = MySqlHelper.GetDataContext())
                {
                    var rnd = new Random();
                    return ctx.Stories.ToList().OrderBy(x => rnd.Next()).Take(3).ToList();
                }
            }
        }

    }
}
